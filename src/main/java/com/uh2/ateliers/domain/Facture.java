package com.uh2.ateliers.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="FACTURE")
public class Facture {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="client_id")
	@JsonIgnore
	private Client client;
	
	private String reference;
	
	private Date dateEcheance;
	
	private double montantTotal;

	public Facture() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Facture(Long id, Client client, String reference, Date dateEcheance, double montantTotal) {
		super();
		this.id = id;
		this.client = client;
		this.reference = reference;
		this.dateEcheance = dateEcheance;
		this.montantTotal = montantTotal;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public Date getDateEcheance() {
		return dateEcheance;
	}

	public void setDateEcheance(Date dateEcheance) {
		this.dateEcheance = dateEcheance;
	}

	public double getMontantTotal() {
		return montantTotal;
	}

	public void setMontantTotal(double montantTotal) {
		this.montantTotal = montantTotal;
	}
	
	
	

}
